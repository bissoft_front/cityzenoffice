/*
| ----------------------------------------------------------------------------------
| TABLE OF CONTENT
| ----------------------------------------------------------------------------------

-Preloader
-Datepicker
-Scale images
-Scroll customization
-Select customization
-Zoom Images
-Slider
-Upload
-Upload face
-Form validation
-Input effects
*/



$(document).ready(function() {

  "use strict";



// PRELOADER //

    var $preloader = $('#page-preloader'),
    $spinner   = $preloader.find('.spinner-loader');
    $spinner.fadeOut();
    $preloader.delay(50).fadeOut('slow');


// DATEPICKER //

  if ($('.input-group.date').length) {
    $('.input-group.date').datepicker({
      language: "uk",
      daysOfWeekHighlighted: "0,6",
      todayHighlight: true
    });
  }



// SCALE IMAGES

  if ($('.img-scale').length) {
    $(function () { objectFitImages('.img-scale') });
  }


// SCROLL CUSTOMIZATION

  if ($('.scroll-pane').length) {
    $('.scroll-pane').jScrollPane();
  }

  $('.js-scrollTrigger').on('click', function (e) {
    e.preventDefault();
    $(this).tab('show');
    $('.tab-content > .tab-pane.active .scroll-pane').show().jScrollPane();
  });

  $('.modal').on('shown.bs.modal', function () {
    $('.tab-content > .tab-pane.active .scroll-pane').show().jScrollPane();
  });



// SELECT CUSTOMIZATION


if ($('.select_box').length) {
   $(function() {
    var $select = $('.select_box').selectpicker({
      noneResultsText: "нічого не знайдено {0}"
    });

    $(':reset').on('click', function(evt) {
        evt.preventDefault();
        var $form = $(evt.target).closest('form');
        $form[0].reset();
        $form.find('select').selectpicker('render')
    });
  })
}


  // ZOOM IMAGES

  if ($('.js-zoom-gallery').length) {
      $('.js-zoom-gallery').each(function() {
          $(this).magnificPopup({
              delegate: '.js-zoom-gallery__item',
              type: 'image',
              gallery: {
                enabled:true
              },
        mainClass: 'mfp-with-zoom',
        zoom: {
          enabled: true,
          duration: 300,
          easing: 'ease-in-out',
          opener: function(openerElement) {
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
          });
      });
    }


  if ($('.js-zoom-images').length) {
      $('.js-zoom-images').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom',
        zoom: {
          enabled: true,
          duration: 300,
          easing: 'ease-in-out',
          opener: function(openerElement) {
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
      });

    }


  if ($('.popup-youtube, .popup-vimeo, .popup-gmaps').length) {
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: false,
      zoom: {
        enabled: true,
        duration: 300
      }
    });
  }

// SLIDER //

  if ($('.js-slick').length) {
    $('.js-slick').slick();
  };



// UPLOAD

  if ($(".js-upload").length){
    $(".js-upload").dropzone({
      url: "../php/index.php",
      clickable: ".js-upload-btn",
      previewsContainer: ".js-upload-container",
      previewTemplate: "<div class='row form-group justify-content-between'><span class='col-auto ui-upload-name' data-dz-name></span><span class='col text-right'><a class='btn-cancel btn btn-default' href='#' data-dz-remove >Cкасувати<svg class='ic' width='30' height='30'><use xlink:href='../svg-symbols.svg#close'></use></svg></a></span></div>"
    });
  }

// Upload face
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#userFace').attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#userInput").change(function(){
      readURL(this);
  });



  /////////////////////////////////////////////////////////////////
  // Тестовая модаль
  /////////////////////////////////////////////////////////////////


  // $( "a, button" ).on( "click", function() {
  //   $('#modalTest').modal();
  // });


  // setTimeout("$('#modalTest').modal()", 5000);

});


// FORM VALIDATION //

window.addEventListener('load', function() {
  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.getElementsByClassName('needs-validation');
  // Loop over them and prevent submission
  var validation = Array.prototype.filter.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  });
}, false);



/////////////////////////////////////////////////////////////////
// INPUT EFFECTS
/////////////////////////////////////////////////////////////////

  (function() {
      if (!String.prototype.trim) {
          (function() {
              var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
              String.prototype.trim = function() {
                  return this.replace(rtrim, '');
              };
          })();
      }

      [].slice.call( document.querySelectorAll( '.js-input' ) ).forEach( function( inputEl ) {
          if( inputEl.value.trim() !== '' ) {
              classie.add( inputEl.parentNode, 'ui-input_filled' );
          }
          inputEl.addEventListener( 'focus', onInputFocus );
          inputEl.addEventListener( 'blur', onInputBlur );
      } );

      function onInputFocus( ev ) {
          classie.add( ev.target.parentNode, 'ui-input_filled' );
      }

      function onInputBlur( ev ) {
          if( ev.target.value.trim() === '' ) {
              classie.remove( ev.target.parentNode, 'ui-input_filled' );
          }
      }
  })();

