

var map, infoBubble;
function init() {
  var mapCenter = new google.maps.LatLng(48.30978575, 37.17038329);
  map = new google.maps.Map(document.getElementById('map-main'), {
    center: mapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    // How zoomed in you want the map to start at (always required)
    zoom: 13,

    scrollwheel: false,


    // How you would like to style the map.
    // This is where you would paste any style found on Snazzy Maps.
    // styles: []
  });

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(48.30978575, 37.17038329)
  });

  // var contentString =
  //   '<a class="map-popover clearfix" href="#">' +
  //     '<span class="map-popover__img">' +
  //       '<img class="img-scale" src="assets/media/content/deputy/48x48/1.jpg", alt="foto">' +
  //     '</span>' +
  //     '<span class="map-popover__inner">' +
  //       '<span class="map-popover__name">Кужель Марія Степанівна</span>' +
  //       '<span class="map-popover__info">Національна комісія, що здійснює державне регулювання у сферах енергетики' +
  //     '</span>' +
  //   '</a>' ;

  // infoBubble = new InfoBubble({
  //   maxWidth: 200,
  //   content: contentString
  // });

  // infoBubble.open(map, marker);


}
google.maps.event.addDomListener(window, 'load', init);
